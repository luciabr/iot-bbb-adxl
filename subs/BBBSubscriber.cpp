#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "MQTTClient.h"
#include "GPIO.h"
#include <json-c/json.h>
#include <iostream>
using namespace std;

#define ADDRESS     "tcp://192.168.0.249:1883"
#define CLIENTID    "bbb_sub1"
#define AUTHMETHOD  "lucia"
#define AUTHTOKEN   "testpwd"
#define TOPIC       "iot/BbbUpdate"
#define QOS         1
#define TIMEOUT     10000L
#define MAX_PITCH   30.0
#define MIN_PITCH   -30.0
#define MAX_ROLL    30.0
#define MIN_ROLL    -30.0

volatile MQTTClient_deliveryToken deliveredtoken;
GPIO gpio = GPIO(60);

void delivered(void *context, MQTTClient_deliveryToken dt) {
    printf("Message with token value %d delivery confirmed\n", dt);
    deliveredtoken = dt;
}

void displayAlert() {
    gpio.setHigh();
}

void resetAlert() {
    gpio.setLow();
}

void checkPitchAndRoll(char* s) {
    cout << "Checking pitch and roll  " << endl;
    double pitch;
    double roll;
    json_object* jobjMsg = json_tokener_parse(s);

    json_object_object_foreach(jobjMsg, key, val) {
        if (strcmp(key, "Pitch") == 0) {
            pitch = json_object_get_double(val);
            cout << "Pitch: " << pitch << endl;
        }
        if (strcmp(key, "Roll") == 0) {
            roll = json_object_get_double(val);
            cout << "Roll: " << roll << endl;
        }
    }
    if (pitch > MAX_PITCH || pitch < MIN_PITCH) {
        cout << "Pitch is too high or too low!" << endl;
        displayAlert();
    }
    if (roll > MAX_ROLL || roll < MIN_ROLL) {
        cout << "Roll is too high or too low!" << endl;
        displayAlert();
    }
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
    int i;
    char* payloadptr;
    printf("Message arrived\n");
    printf("     topic: %s\n", topicName);
    payloadptr = (char*) message->payload;
    checkPitchAndRoll(payloadptr);
    printf("   message: \n");
    for(i=0; i<message->payloadlen; i++) {
        putchar(*payloadptr++);
    }
    putchar('\n');
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

void connlost(void *context, char *cause) {
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);
}


int main(int argc, char* argv[]) {
    resetAlert();
    MQTTClient client;
    MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
    int rc;
    int ch;

    MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    opts.keepAliveInterval = 20;
    opts.cleansession = 1;
    opts.username = AUTHMETHOD;
    opts.password = AUTHTOKEN;

    MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered);
    if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
        printf("Failed to connect, return code %d\n", rc);
        exit(-1);
    }
    printf("Subscribing to topic %s\nfor client %s using QoS%d\n\n"
           "Press Q<Enter> to quit\n\n", TOPIC, CLIENTID, QOS);
    MQTTClient_subscribe(client, TOPIC, QOS);

    do {
        ch = getchar();
        if (ch == 'r' || ch == 'R') {
            cout << "Resetting alarm..." << endl;
            resetAlert();
        }
    } while(ch!='Q' && ch != 'q');
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    return rc;
}

