#include "GPIO.h"
#include<iostream>
#include<fstream>
#include<unistd.h>
using namespace std;
#define GPIO_PATH "/sys/class/gpio/"

GPIO::GPIO(int gpioNumber) {
    this->gpioNumber = gpioNumber;
    gpioPath = string(GPIO_PATH "gpio") + to_string(gpioNumber) + string("/");
    writeSysfs(string(GPIO_PATH), "export", to_string(gpioNumber));
    usleep(100000); // ensure GPIO is exported
    writeSysfs(gpioPath, "direction", "out");
    writeSysfs(gpioPath, "value", "0");
}

void GPIO::setHigh() {
    writeSysfs(gpioPath, "value", "1");
}

void GPIO::setLow() {
    writeSysfs(gpioPath, "value", "0");
}

void GPIO::displayState() {
    ifstream fs;
    fs.open((gpioPath + "value").c_str());
    string line;
    cout << "The current GPIO state is ";
    while(getline(fs,line)) cout << line << endl;
    fs.close();
}

GPIO::~GPIO() {
    writeSysfs(string(GPIO_PATH), "unexport", to_string(gpioNumber));
}

void GPIO::writeSysfs(string path, string filename, string value) {
    ofstream fs;
    fs.open((path+filename).c_str());
    fs << value;
    fs.close();
}
