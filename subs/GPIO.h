#ifndef EE513_RTC_GPIO_H
#define EE513_RTC_GPIO_H

#include<string>
using std::string;

class GPIO {
private:
    string gpioPath;
    int gpioNumber;
    void writeSysfs(string path, string filename, string value);
public:
    GPIO(int gpioNumber);
    virtual void setHigh();
    virtual void setLow();
    virtual void displayState();
    virtual ~GPIO();
};


#endif //EE513_RTC_GPIO_H
