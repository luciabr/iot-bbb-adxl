// Based on the Paho C code example from www.eclipse.org/paho/
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
#include <ctime>
#include <unistd.h>
#include <pthread.h>
#include "MQTTClient.h"
#include "ADXL345.h"
#define  CPU_STAT "/proc/stat"
using namespace std;
using namespace exploringBB;

//Please replace the following address with the address of your server
#define ADDRESS    "tcp://192.168.0.249:1883"
#define CLIENTID   "bbb_pub"
#define AUTHMETHOD "lucia"
#define AUTHTOKEN  "testpwd"
#define TOPIC      "iot/BbbUpdate"
#define QOS        1
#define TIMEOUT    10000L
#define WILL_MSG    "I'm dead"
#define ITERATIONS 100

void getCpuUtilization(char* cpuUtilization) {
    string stats;
    ifstream cpuStat (CPU_STAT);
    getline (cpuStat, stats);
    cpuStat.close();

    stats = stats.substr(5);
    stringstream statsStream(stats);
    string n;

    unsigned int sum = 0;
    unsigned int idle;
    for (int i=1; i<=10; i++) {
        getline(statsStream, n, ' ');
        sum += stoi(n);
        if (i == 4) {
            idle = stoi(n);
        }
    }
    float uptime = 100.0 * (1.0 - (float)idle/sum);

    sprintf(cpuUtilization, "\"CPUUtilization\": %f", uptime);
}

void getADXL345Status(char* adxl345Status, unsigned int I2CBus, unsigned int I2CAddress) {
    ADXL345 sensor (2,0x53);
    sensor.setResolution(ADXL345::NORMAL);
    sensor.setRange(ADXL345::PLUSMINUS_4_G);
    sensor.readSensorState();
    sprintf(adxl345Status, "\"Pitch\": %f, \"Roll\": %f, \"AccX\": %d, \"AccY\": %d, \"AccZ\": %d",
            sensor.getPitch(), sensor.getRoll(),
            sensor.getAccelerationX(), sensor.getAccelerationY(), sensor.getAccelerationZ());
    return;
};

void getTime(char* datetime) {
    time_t now = time(0);
    tm* localTime = localtime(&now);
    char date[10];
    sprintf(date, "20%02d-%02d-%02d", localTime->tm_year-100, localTime->tm_mon+1, localTime->tm_mday);
    sprintf(datetime, " \"Date\": \"%s\", \"Hour\": %d, \"Min\": %d, \"Sec\": %d",
            date, localTime->tm_hour, localTime->tm_min, localTime->tm_sec);
}

int main(int argc, char* argv[]) {
    char str_payload[300];          // Set your max message size here
    char cpuUtilization[30];
    char adxl345Status[100];
    char dateTime[100];
    MQTTClient client;
    MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    opts.keepAliveInterval = 20;
    opts.cleansession = 1;
    opts.username = AUTHMETHOD;
    opts.password = AUTHTOKEN;
    MQTTClient_willOptions willOpts = MQTTClient_willOptions_initializer;
    opts.will = &willOpts;
    willOpts.topicName = TOPIC;
    willOpts.message = WILL_MSG;
    int rc;
    if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
        cout << "Failed to connect, return code " << rc << endl;
        return -1;
    }

    int count = 0;
    while(count < ITERATIONS){
        getCpuUtilization(cpuUtilization);
        getADXL345Status(adxl345Status, 2, 0x53);
        getTime(dateTime);
        sprintf(str_payload, "{%s, %s, %s}", cpuUtilization, adxl345Status, dateTime);

        pubmsg.payload = str_payload;
        pubmsg.payloadlen = strlen(str_payload);
        pubmsg.qos = QOS;
        pubmsg.retained = 0;
        MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token);
        cout << "Waiting for up to " << (int)(TIMEOUT/1000) <<
             " seconds for publication of " << str_payload <<
             " \non topic " << TOPIC << " for ClientID: " << CLIENTID << endl;
        rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
        cout << "Message with token " << (int)token << " delivered." << endl;
        usleep(100000);
        count++;
    }
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    return rc;
}


